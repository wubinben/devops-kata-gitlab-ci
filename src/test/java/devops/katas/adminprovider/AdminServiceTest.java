package devops.katas.adminprovider;

import org.assertj.core.api.BDDAssertions;
import org.junit.jupiter.api.Test;

public class AdminServiceTest {
    @Test
    public void should_retrieve_an_admin() {
        AdminService adminService = new AdminService();

        Admin admin = adminService.retrieveAdmin(4);

        BDDAssertions.then(admin.getFirstName()).isEqualTo("First4");
        BDDAssertions.then(admin.getLastName()).isEqualTo("Last4");
    }
}